import json
import os
import sys


def main():
    get_ipython().system_raw(
        'tensorboard --logdir {} --host 0.0.0.0 --port 6006 &'
        .format(config["LOG_DIR"])
    )
    get_ipython().system_raw('{} http 6006 &'.format(config["ngrok"]))

def read_config():
    os.chdir(sys.path[0])
    os.chdir('..')
    config_path = os.getcwd() + '/object_detection/config.cfg'
    global config
    config = {}
    with open(config_path, newline="") as f:
        for line in f:
            (key, val) = line.strip().partition("=")[::2]
            config[key] = val

read_config()
main()