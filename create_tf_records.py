from __future__ import division, print_function, absolute_import

import pandas as pd

import pkg_resources
import numpy as np
import csv

import re
import os
import io
import glob
import shutil
import urllib.request
import tarfile
import xml.etree.ElementTree as ET

import tensorflow.compat.v1 as tf

from PIL import Image
from collections import namedtuple, OrderedDict

from object_detection.utils import dataset_util


def class_text_to_int(row_label):
  if row_label == 'HDD':
    return 1
  else:
    return None
    # man kann gar nicht None returnen?

def split(df, group):
  data = namedtuple('data', ['filename', 'object'])
  # additional line to original code, or else it would try to read a .jpg.jpg file as it seems
  # df['filename'] = df['filename'].str.replace(r'.jpg$', '')
  gb = df.groupby(group)  
  return [data(filename, gb.get_group(x)) for filename, x in zip(gb.groups.keys(), gb.groups)]

def create_tf_example(group, path):
	with tf.io.gfile.GFile(os.path.join(path, '{}'.format(group.filename[:-4])), 'rb') as fid:
	  encoded_jpg = fid.read()
	encoded_jpg_io = io.BytesIO(encoded_jpg)
	image = Image.open(encoded_jpg_io)
	width, height = image.size
	filename = group.filename.encode('utf8')
	image_format = b'jpg'
	xmins = []
	xmaxs = []
	ymins = []
	ymaxs = []
	classes_text = []
	classes = []

	for index, row in group.object.iterrows():
		xmins.append(row['xmin'] / width)
		xmaxs.append(row['xmax'] / width)
		ymins.append(row['ymin'] / height)
		ymaxs.append(row['ymax'] / height)
		classes_text.append(row['class'].encode('utf8'))    
		classes.append(class_text_to_int(row['class']))


	tf_example = tf.train.Example(features=tf.train.Features(feature={
		'image/height': dataset_util.int64_feature(height),
		'image/width': dataset_util.int64_feature(width),
		'image/filename': dataset_util.bytes_feature(filename),
		'image/source_id': dataset_util.bytes_feature(filename),
		'image/encoded': dataset_util.bytes_feature(encoded_jpg),
		'image/format': dataset_util.bytes_feature(image_format),
		'image/object/bbox/xmin': dataset_util.float_list_feature(xmins),
		'image/object/bbox/xmax': dataset_util.float_list_feature(xmaxs),
		'image/object/bbox/ymin': dataset_util.float_list_feature(ymins),
		'image/object/bbox/ymax': dataset_util.float_list_feature(ymaxs),
		'image/object/class/text': dataset_util.bytes_list_feature(classes_text),
		'image/object/class/label': dataset_util.int64_list_feature(classes),
		}))
	return tf_example
  
def main(object_detection):
    
    #change this to the base directory where your data/ is 
    data_base_url = object_detection+'/data/'

    #location of images	
    image_dir = data_base_url +'images/'

    #creates tfrecord for both csv's
    for csv in ['train_labels', 'test_labels']:
      writer = tf.io.TFRecordWriter(data_base_url + csv + '.record')
      path = os.path.join(image_dir)
      examples = pd.read_csv(data_base_url + csv + '.csv')
      grouped = split(examples, 'filename')
      for group in grouped:
        tf_example = create_tf_example(group, path)
        writer.write(tf_example.SerializeToString())
        
      writer.close()
      output_path = os.path.join(os.getcwd(), data_base_url + csv + '.record')
      print('Successfully created the TFRecords: {}'.format(data_base_url +csv + '.record'))
