from __future__ import division, print_function, absolute_import

import pandas as pd

import os
import glob
import shutil
import urllib.request
import tarfile
import xml.etree.ElementTree as ET

import create_tf_records as creating_tf_records
import json

import sys


#####################
## main
#####################
def main():
    read_config()
    create_label_map()
    download_pretrained_model()
    create_tf_records(config["object_detection"])


def read_config():
    global MODELS_CONFIG
    MODELS_CONFIG = {}
    print(sys.path[0])
    with open(sys.path[0] + '/models_config.cfg', "r") as read_file:
        MODELS_CONFIG = json.load(read_file)
    print("Model configuration")
    print(MODELS_CONFIG)

    os.chdir(sys.path[0])
    os.chdir('..')
    config_path = os.getcwd() + '/object_detection/config.cfg'
    global config
    config = {}
    with open(config_path, newline="") as f:
        for line in f:
            (key, val) = line.strip().partition("=")[::2]
            config[key] = val
    print("Configuration loaded")
    print(config)


def create_label_map():
    # adjusted from: https://github.com/datitran/raccoon_dataset
    def xml_to_csv(path):
        classes_names = []
        xml_list = []

        for xml_file in glob.glob(path + '/*.xml'):
            tree = ET.parse(xml_file)
            root = tree.getroot()
            for member in root.findall('object'):
                classes_names.append(member[0].text)
                value = (root.find('filename').text + '.jpg',
                         int(root.find('size')[0].text),
                         int(root.find('size')[1].text),
                         member[0].text,
                         int(member[4][0].text),
                         int(member[4][1].text),
                         int(member[4][2].text),
                         int(member[4][3].text))
                xml_list.append(value)
        column_name = ['filename', 'width', 'height', 'class', 'xmin', 'ymin', 'xmax', 'ymax']
        xml_df = pd.DataFrame(xml_list, columns=column_name)
        classes_names = list(set(classes_names))
        classes_names.sort()
        return xml_df, classes_names

    for labels in ['train_labels', 'test_labels']:
        label_path = os.path.join(config['data'], labels)  # ToDo
        xml_df, classes = xml_to_csv(label_path)
        xml_df.to_csv(f'{label_path}.csv', index=None)
        print(f'Successfully converted {label_path} xml to csv.')

    label_map_path = os.path.join(config['data'], "label_map.pbtxt")
    pbtxt_content = ""

    for i, class_name in enumerate(classes):
        pbtxt_content = (
                pbtxt_content
                + "item {{\n    id: {0}\n    name: '{1}'\n}}\n\n".format(i + 1, class_name)
        )
    pbtxt_content = pbtxt_content.strip()
    with open(label_map_path, "w") as f:
        f.write(pbtxt_content)


def download_pretrained_model():
    DEST_DIR = config['object_detection'] + '/models/research/pretrained_model'

    # Name of the object detection model to use.
    MODEL = MODELS_CONFIG[config['selected_model']]['model_name']

    # selecting the model
    MODEL_FILE = MODEL + '.tar.gz'

    # creating the downlaod link for the model selected
    DOWNLOAD_BASE = 'http://download.tensorflow.org/models/object_detection/'

    # checks if the model has already been downloaded, download it otherwise
    if not (os.path.exists(MODEL_FILE)):
        print('### download pretrained ' + MODEL + ' model ###')
        urllib.request.urlretrieve(DOWNLOAD_BASE + MODEL_FILE, MODEL_FILE)

    # unzipping the model and extracting its content
    tar = tarfile.open(MODEL_FILE)
    tar.extractall()
    tar.close()

    # creating an output file to save the model while training
    # os.remove(MODEL_FILE)
    if (os.path.exists(DEST_DIR)):
        shutil.rmtree(DEST_DIR)
    os.rename(MODEL, DEST_DIR)


def create_tf_records(object_detection):
    print(object_detection)
    creating_tf_records.main(object_detection)


main()
