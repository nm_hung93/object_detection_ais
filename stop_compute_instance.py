from azureml.core import Workspace

# Compute Instance
# https://docs.microsoft.com/en-us/python/api/azureml-core/azureml.core.compute.computeinstance(class)?view=azure-ml-py
from azureml.core.compute import ComputeInstance
from IPython.core.display import HTML

def main():
    stop_compute_instance()
    
def stop_compute_instance():
    ws = Workspace.from_config(path=".file-path/ws_config.json")
    print(ws.list_connections)
    computer_name = ComputeInstance.list(workspace=ws)[0].name
    #display(ComputeInstance(ws, computer_name))
    ComputeInstance(ws, computer_name).stop(wait_for_completion=False, show_output=True)
    
main()

    
    
   