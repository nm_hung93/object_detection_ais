#!/bin/bash
# landing (required)
# object_detection_ais (repo)
# object_detection (generated)
# -- data
# -- -- images
# -- -- annotations
# -- -- train_labels
# -- -- test_labels

training_name="Testi test"
selected_model='faster_rcnn_inception_v2_coco'
train_data_rate="1"
num_steps=70000
num_classes=1
gpu=1
tensorflow_version='1.13.1'
email_address='moritz.drobnitzky@web.de'
email_recipient='Moritz'

function help {

   echo "Using the tutorial mentioned below, a neuronal network for object detection can be trained."
   echo "The purpose of this script is to automate the most basic steps until a model training can be performed."
   echo "Final adjustments should be done within the above attributes and the create_tf_records.py inside the "
   echo "class_to_int function depending on the number of classes."
   echo "source: https://towardsdatascience.com/detailed-tutorial-build-your-custom-real-time-object-detector-5ade1017fd2d#d582"
   echo
   echo "Syntax: ./start.sh <init|board|train|reset> [ -c <all|init|model|label> ]"
   echo "options:"
   echo "init         Downloads all necessary dependencies, takes the sample pipline [selected_model] from the tensorflow repo"
   echo "             and configures it as specified. Further adjustments can be done before running the training"
   echo "board  Starts the tensorboard to monitor the training progress."
   echo "train        Starts the training process after all initial processes have been finished. After training has been finished"
   echo "             the trained model as well as the raw data gets wrapped up and zipped."
   echo "reset        Removes trained model as well as split data, flags and configuration to enable a new clean setup "
   echo ""
   echo " -c          cleans the flags set by the init process. The init process goes through 3 steps:"
   echo "             [init]: downloads dependencies, clones TF and runs protobuf"
   echo "             [model]: takes sample config for specified pipeline and tweaks it as configured above"
   echo "             [label]: copies and splits data within the landing folder as well as creates tf_records and labelmappigns"

}

# Do Not Change Anything From Here Onwards

git=$(pwd)
cd ..
root=$(pwd)
object_detection=$root/object_detection
data=$object_detection/data
LOG_DIR=$object_detection/models/research/training/
ngrok=${object_detection}/models/research/ngrok
landing_data=$root/landing
landing_test_data=$root/landing_test



set -e
while getopts "c:l:" opt; do
        case "${opt}" in
              c) case $OPTARG in
                  all) echo "remove all SUCCESS flags - need to download and configure project anew"
                        rm $object_detection/*SUCCESS ;;
                  init) echo "remove all SUCCESS flags - need to download and configure project anew"
                        rm $object_detection/1_dependencies.SUCCESS ;;
                  model) echo "removing model.SUCCESS flag - need to configure model anew"
                        rm $object_detection/2_model.SUCCESS;;
                  label) echo "removing label.SUCCESS flag - need to split data and create tf records anew"
                        rm $object_detection/3_labels.SUCCESS;;
                  *) echo "options may contain <init|model|label> "
                     exit;;
                  esac
                  ;;
              *) echo "options may contain <init|model|label> "
                 exit
                ;;
        esac
  done

function main  {

  case $1 in
    init)         init_project;;
    board)        start_tensorboard;;
    train)        train_model "$2" ;;
    reset)        reset_project ;;
    debug)        debug;;
    *)            help;;
  esac

}

#############################################################
#############################################################
## main-functions
#############################################################
#############################################################
function debug {
  send_email "$email_text"
}
function init_project {

  if [[ ! -f $object_detection/1_dependencies.SUCCESS ]]; then
    write_log '### START INIT PROCESS, DOWNLOADING DEPENDENCIES ###'
    download_dependencies
	  touch $object_detection/1_dependencies.SUCCESS
  fi

  export_pythonpath
  create_config_file

  if [[ ! -f $object_detection/2_model.SUCCESS ]]; then
    write_log '### COPY MODEL PIPELINE TEMPLATE ###'
    configure_model_pipeline
	  touch $object_detection/2_model.SUCCESS
  fi

  if [[ ! -f $object_detection/3_labels.SUCCESS ]]; then
    if [ -d $data ]; then
      write_log "### Removing split data ###"
      rm -r ${data:?"path for data not filled, error in script"}
    fi
    write_log '### START SPLITTING DATA AND CREATE TF-RECORDS ###'
    split_data
    python $git/run.py
    touch $object_detection/3_labels.SUCCESS
  fi



}
function start_tensorboard {
  write_log "tensorboard starting"
  write_log "please start tensorboard manually as following"
  mkdir -p $LOG_DIR

  write_log "get_ipython().system_raw(
        'tensorboard --logdir {} --host 0.0.0.0 --port 6006 &'
        .format(\"$LOG_DIR\")
    )"
  write_log "get_ipython().system_raw('{} http 6006 &'.format(\"$ngrok\"))"

  #ipython $git/run_tensorboard.py

  write_log "!curl -s http://localhost:4040/api/tunnels | python3 -c  \"import sys, json; print(json.load(sys.stdin)['tunnels'][0]['public_url']) \" "
}

function train_model {
 tensorboard_link=$1
 write_log "$tensorboard_link"
 check_success
 export_pythonpath
 write_log "### RUN TRAINING with $selected_model ###"

 email_text="Starting Training"
 send_email "$email_text" "$tensorboard_link"
 python $object_detection/models/research/object_detection/model_main.py \
     --pipeline_config_path=$object_detection/${selected_model}.config \
     --model_dir=$LOG_DIR

 email_text="Finished Training"
 send_email "$email_text" "$tensorboard_link"
 write_log "### TRAINING FINISHED - START ZIPPING TRAINED MODEL AND RAW DATA ###"
 zip_training_result
 
 write_log "finished zipping"
 
 #stopping compute-instance by Maik, implemented by Moritz
 write_log "### STOPPING COMPUTE INSTANCE ###"
 python $git/stop_compute_instance.py
}

function reset_project {
  #https://stackoverflow.com/questions/25751030/how-to-get-only-process-id-in-specify-process-name-in-linux
  # grep -v grep to ignore grep command
  tensorboard_pid=$(ps -ef | grep -v grep | grep tensorboard | awk '{print $2}')

  if [ -z $tensorboard_pid ]; then
    write_log "### no running tensorboard ###"
  else
    write_log "### shuting down tensorboard ###"
    kill -9 $tensorboard_pid
  fi


  if [ -d $LOG_DIR ]; then
    write_log "### Removing trained model ###"
    rm -r $LOG_DIR
  fi

  if [ -d $data ]; then
    write_log "### Removing split data ###"
    rm -r ${data:?"path for data not filled, error in script"}
  fi

  write_log "### Removing flags model and label - need to ./start.sh init again ###"
  rm $object_detection/2_model.SUCCESS
  rm $object_detection/3_labels.SUCCESS
}



#############################################################
#############################################################
## sub-functions
#############################################################
#############################################################

#############################################################
### 1. init project
#############################################################
function download_dependencies {

	mkdir -p $object_detection
    cd $object_detection

	write_log 'import dependencies'
	# if pycocotools fails to install, you need python headers first
	sudo apt-get update
	sudo apt-get install  protobuf-compiler python-pil python-lxml python-tk bc -y
	python -m pip install  Cython contextlib2 pillow lxml matplotlib pycocotools
	python -m pip install numpy==1.16
	if [[ $gpu -eq 1 ]]; then
	  python -m pip install tensorflow-gpu==$tensorflow_version
	  write_log "installed Tensorflow GPU Version"
  else
    python -m pip install tensorflow==$tensorflow_version
    write_log "installed Tensorflow non GPU Version"
  fi

  if [[ ! -d models ]]; then
    write_log 'download TF from git repo'
    git clone -b r1.13.0 https://github.com/tensorflow/models
  fi

  pushd $object_detection/models/research > /dev/null
  if [[ ! -f protobuf.zip ]]; then
    wget -O protobuf.zip https://github.com/google/protobuf/releases/download/v3.0.0/protoc-3.0.0-linux-x86_64.zip
    unzip protobuf.zip
  fi
  ./bin/protoc object_detection/protos/*.proto --python_out=.

  if [[ ! -f ngrok-stable-linux-amd64.zip ]]; then
    write_log 'download Tensorboard'
    wget https://bin.equinox.io/c/4VmDzA7iaHb/ngrok-stable-linux-amd64.zip
    unzip -o ngrok-stable-linux-amd64.zip
  fi
  popd > /dev/null
}


function split_data {
    mkdir -p $data/images/ $data/annotations/ $data/test_labels $data/train_labels
    
  if [[ $train_data_rate -lt 1 ]]; then    
   # use rsync to avoid no data format in folder errors
    rsync -av --ignore-missing-args $landing_data/*.xml $data/annotations/
    rsync -av --ignore-missing-args $landing_data/*.jpg $data/images/
    rsync -av --ignore-missing-args $landing_data/*.jpeg $data/images/

    amount_data=$(ls $data/annotations | wc -l)
    amount_train_data=$(echo "scale=1;$amount_data*$train_data_rate" | bc | cut -d'.' -f1)

    ls $data/annotations/* | sort -R | head -$amount_train_data | xargs -I{} mv {} $data/train_labels/
    ls $data/annotations/* | xargs -I{} mv {} $data/test_labels/
  else
    write_log "taking train data from $landing_data and test data from $landing_test_data"
    
    rsync -av --ignore-missing-args $landing_data/*.xml $data/train_labels/    
    rsync -av --ignore-missing-args $landing_data/*.jpg $data/images/
    rsync -av --ignore-missing-args $landing_data/*.jpeg $data/images/
    
    rsync -av --ignore-missing-args $landing_test_data/*.xml $data/test_labels/    
    rsync -av --ignore-missing-args $landing_test_data/*.jpg $data/images/
    rsync -av --ignore-missing-args $landing_test_data/*.jpeg $data/images/

    amount_train_data=$(ls $data/train_labels/ |  wc -l)
    amount_data=$(ls $data/images/ |  wc -l)
  fi

    write_log "amount of data:$amount_data"
    write_log "amount of train-data:$amount_train_data"

}

function export_pythonpath {
    if [[ -z $PYTHONPATH ]]; then
      export PYTHONPATH="$object_detection/models/research/:$object_detection/models/research/slim"
    else
      export PYTHONPATH="${PYTHONPATH}:$object_detection/models/research/:$object_detection/models/research/slim"
    fi
}

function create_config_file {

  write_log "creating config file at $object_detection"
  cat <<- EOF > $object_detection/config.cfg
  object_detection=$object_detection
  data=$data
  pythonpath=$object_detection/models/research/:$object_detection/models/research/slim
  selected_model=$selected_model
  train_data_rate=$train_data_rate
  LOG_DIR=$LOG_DIR
  ngrok=$ngrok

EOF
}

#############################################################
### 2. configure model
#############################################################

function configure_model_pipeline {
  # copy and replace trainign pipline
	cp $object_detection/models/research/object_detection/samples/configs/${selected_model}.config $object_detection

	# weil die namen der labelmaps und records in den sample Dateien generisch sind müssen wir sie für uns zurechtformatieren
  sed -i "s|label_map_path: \"PATH_TO_BE_CONFIGURED.*|label_map_path: \"PATH_TO_BE_CONFIGURED/data/label_map.pbtxt\"|g" $object_detection/${selected_model}.config
  sed -i "s|input_path: \"PATH_TO_BE_CONFIGURED.*train.*|input_path: \"PATH_TO_BE_CONFIGURED/data/train_labels.record\"|g" $object_detection/${selected_model}.config
  sed -i "s|input_path: \"PATH_TO_BE_CONFIGURED.*val.*|input_path: \"PATH_TO_BE_CONFIGURED/data/test_labels.record\"|g" $object_detection/${selected_model}.config

  sed -i "s|fine_tune_checkpoint:.*|fine_tune_checkpoint: \"PATH_TO_BE_CONFIGURED/models/research/pretrained_model/model.ckpt\"|g" $object_detection/${selected_model}.config

  # ersetzt schlussendlich PATH_TO_BE_CONFIGURED mit dem Pfad zum Projekt
	sed -i "s|PATH_TO_BE_CONFIGURED|${object_detection}|g" $object_detection/${selected_model}.config

  # ersetzt die für das Training spezifischen Parameter
  sed -i "s|num_classes:.*|num_classes: ${num_classes}|g" $object_detection/${selected_model}.config
  sed -i "s|num_steps:.*|num_steps: ${num_steps}|g" $object_detection/${selected_model}.config
  #ToDo: batch_size
  # fügt Zeile für Visualisierung in Tensorboard hinzu
  sed -i "s|eval_config: {|eval_config: { \n  num_visualizations: 20 |g" $object_detection/${selected_model}.config

}

#############################################################
### 3. train model
#############################################################

function zip_training_result {

  tar cfzv $object_detection/$(date '+%Y%m%d-%H%M')_"${training_name// /-}"_result_${selected_model}_${amount_data}_${train_data_rate}_${num_steps}_${num_classes}.tar.gz \
  -C $object_detection/ ${selected_model}.config \
  -C $root landing  \
  -C $root landing_test \
  -C $object_detection/models/research/ training/ 
  

}

#############################################################
### helper
#############################################################

function write_log {

  local content=$1
  local type="INFO"


   if [[ $# -eq 2 ]]; then
        type="$( echo $2 | tr [:lower:] [:upper:] )"
    fi

   echo "[$type] [$( date "+%Y%m%d %T" )] $1"
}

function check_success {

  # don't start training if init process did not finished successfully / flags missing
  if [[ ! -f $object_detection/1_dependencies.SUCCESS  ]] || \
      [[ ! -f $object_detection/2_model.SUCCESS  ]] || \
      [[ ! -f $object_detection/3_labels.SUCCESS  ]]; then
    write_log "please start the init process" Error
    exit
  fi


}

function send_email {
  text=$1
  link=$2

  if [[ ! -z $email_address ]] && [[ ! -z $email_recipient ]]; then
    cp $git/mail_template.txt $git/mail.txt
    sed -i "s|\[NAME\]|\"$email_recipient\"|" $git/mail.txt
    sed -i "s|\[EMAIL\]|<$email_address>|" $git/mail.txt
    sed -i "s|\[LINK\]|$link|" $git/mail.txt
    sed -i "s|\[TEXT\]|$text|" $git/mail.txt
    curl --url 'smtps://smtp.gmail.com:465' --ssl-reqd --mail-from 'seminar.objectdetection@gmail.com' --mail-rcpt $email_address --upload-file $git/mail.txt --user 'seminar.objectdetection@gmail.com:Apfelsaft123!' --insecure
  else
    write_log "no email configured, cant send email but will continue script "
  fi


}

function param_checks {

  if (( $( echo "$train_data_rate != 1"| bc -l ) )); then
    write_log "The train_data_rate is set to 1 meaning you need an extra folder for test data at $landing_test_data which contains all testing data for model optimization."
    read -r -p "Are you sure that is the setup you configured? [y/N] " response
    if [[ "$response" =~ ^([nN][oO]|[nN])$ ]]; then
        exit
    fi
    if [[ ! -d $landing_test_data ]]; then
        write_log "It is expected, that a directory named landing exists in $root which contains all labels and images of the images used for model optimzation" Error
        exit
    fi
  fi

 # deactivated check - cant enter input in jupyter notebook
 # if [[  -z $email_address ]] && [[  -z $email_recipient ]]; then
 #   write_log "no email configured, let's do this now or cancel the script with ctrl+c or proceed without email notification with enter"
 #   write_log "email address to send to"
 #   read email_address
 #   write_log "name of recipient"
 #   read email_recipient
 # fi

  if [[ ! -d $root/landing ]]; then
    write_log "It is expected, that a directory named landing exists in $root which contains all labels and images" Error
    exit
  fi
}

#############################################################
### starting main
#############################################################

param_checks
main $1 $2


